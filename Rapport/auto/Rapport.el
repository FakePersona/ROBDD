(TeX-add-style-hook
 "Rapport"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("fontenc" "T1") ("inputenc" "utf8") ("babel" "main=francais" "english")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art10"
    "fullpage"
    "fontenc"
    "inputenc"
    "babel"
    "amsmath"
    "amssymb"))
 :latex)

