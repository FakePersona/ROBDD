(* Example of file when using the compiles mode *) 

open Lib
open Formulas
open Prop
open Bdd

let maxNode = 2000;;

(* Pretty-printer for formulas, to be used with compiled mode *)
let print_formula fm = print_prop_formula fm; print_newline();;

(* Our make function! *)

let make tt th i l hi =
  if (l = hi) then
    l
  else if (member th i l hi) then
    lookup th i l hi
  else
      let node = add tt i l hi in
      begin
        insert th i l hi node;
        node
      end;;

(* Now, to apply negations *)

let apply_neg t ht u =
  let seen = Array.make maxNode (-1) in (* Array used for dynamic programming *)
  let rec neg_aux u =
    if seen.(u) = (-1) then     (* We have not already seen that node *)
      begin

        let i = var t u in

        let lo = low t u in
        let l =                 (* Computing lower son *)
            if (lo > 1) then
              neg_aux lo
            else
              ((lo + 1) mod 2)
        in

        let hio = high t u in
        let hi =                (* Computing higher son *)
          if (hio > 1) then
            neg_aux hio
          else
            ((hio + 1) mod 2)
        in

        let neg_u = make t ht i l hi in
        seen.(u) <- neg_u;
        neg_u
      end
    else                        (* That node has already been treated *)
        seen.(u)
  in
  neg_aux u;;

(* Now we get on with the coding of the applying function without dynamic programming for the time being*)

let apply t ht p u v =
  let seen = Array.make_matrix maxNode maxNode (-1)  in (* Dynamic programming *)
  let rec apply_aux u v =
    (* Have we seen that association already ? *)
    if seen.(u).(v) <> -1 then
      seen.(u).(v)
    else
      let iu = var t u in
      let iv = var t v in
      if (u < 2 && v < 2) then
        (*  Is it a basic boolean operation between leaves ? *)
        begin
          seen.(u).(v) <-  (match p with
                           |Ou -> if not (u = 0 && v = 0) then 1 else 0
                           |Et -> if (u = 1 && v = 1) then 1 else 0
                           |Impl -> if (u = 0 || v = 1) then 1 else 0
                           |Equiv -> if (u = v) then 1 else 0);
                                     seen.(u).(v)
        end
      else
        if (u <= 1) then
          (* Base recursion case *)
          begin
            seen.(u).(v) <- (match p with
                             |Ou -> if (u = 1) then u else v
                             |Et -> if (u = 0) then u else v
                             |Impl -> if (u = 0) then 1 else v;
                             |Equiv -> if (u = 1) then v else apply_neg t ht v);
            seen.(u).(v)
          end
        else
          if (v <= 1) then
          (* base recursion, part two *)
          begin
            seen.(u).(v) <- (match p with
                            |Ou -> if (v = 1) then v else u
                            |Et -> if (v = 0) then v else u
                            |Impl -> if (v = 1) then 1 else apply_neg t ht u
                            |Equiv -> if (v = 1) then u else apply_neg t ht u);
                                      seen.(u).(v)
          end
          else if (iu = iv) then
            (* Are we in proposition 5 's instance ? *)
            begin
              seen.(u).(v) <- (make t ht iu (apply_aux (low t u) (low t v)) (apply_aux (high t u) (high t v)));
              seen.(u).(v)
            end
              (* Then we are in proposition 4's instance *)
          else if (iu < iv) then
            begin
              seen.(u).(v) <- (make t ht iu (apply_aux (low t u) v) (apply_aux (high t u) v));
              seen.(u).(v)
            end
              (* We are inverting the order of formulas to respect variable order *)
          else
            begin
              seen.(u).(v) <- (make t ht iv (apply_aux u (low t v)) (apply_aux u (high t v)););
              seen.(u).(v)
            end
  in

  apply_aux u v;;

(* Now... To actually make the building function *)

let build t ht f =
  let rec build_aux  f = match f with
    |False -> 0
    |True -> 1
    |Atom(P(a)) -> make t ht a 0 1
    |Not(a) -> apply_neg t ht (build_aux a)
    |Or(a,b) -> apply t ht Ou (build_aux a) (build_aux b)
    |And(a,b) -> apply t ht Et (build_aux a) (build_aux b)
    |Imp(a,b) -> apply t ht Impl (build_aux a) (build_aux b)
    |Iff(a,b) -> apply t ht Equiv (build_aux a) (build_aux b)
  in
  build_aux f;;

(* sat, valid and anysat *******************)

(* By unicty of ROBDD, a simple test is enough for sat and valid *)
let sat t i = not (i=0);;
let valid i = (i=1);;

(* type used by anysat's auxiliary function *)
type anysattype =
	|NoSolution
	|Solution of (variable * bool) list;;

let anysat t i =
	let rec auxanysat i =
		if isZero i then NoSolution
		else if isOne i then Solution([])
		else
			let s0 = auxanysat (low t i) in
			match s0 with
			|Solution(l) -> Solution((var t i, false)::l)
			|NoSolution -> let s1 = auxanysat (high t i) in
				match s1 with
				|Solution(l) -> Solution((var t i, true)::l)
				|NoSolution -> NoSolution in
	match auxanysat i with
	|Solution(l) -> l;
	|NoSolution -> failwith "NoSolution";;

(* **************************************************** *)
(* Trying my hand at the n queens problem formalisation *)
(* **************************************************** *)

(* I think nodes variables are locked to ints, so w/e *)
let index n i j =
  i * n + j;;

(* The actual function that will give us the necessary logic formula *)
let make_queens n =
  let formula = ref True in     (* we are going to fill up this formule throughout this execution *)

  (* We know there is at least one queen par line *)
  for i = 0 to (n-1) do
    let line_formula = ref (Atom(P(index n i 0))) in
    for j =1 to (n-1) do
      line_formula := (Or(!line_formula,Atom(P(index n i j))))
    done;
    formula := And(!formula,!line_formula)
  done;


  for i = 0 to (n-1) do
    for j = 0 to (n-1) do

      (* There can only be one. Per line *)
      let line_only = ref True in
      for k = 0 to (n-1) do
        if k <> j then
          line_only := (And(!line_only, Not(Atom(P(index n i k)))))
      done;
      formula := And(!formula,Imp(Atom(P(index n i j)), !line_only));

      (* Columns, lines *)
      let col_only = ref True in
      for k = 0 to (n-1) do
        if k <> i then
          col_only := And(!col_only, Not(Atom(P(index n k j))))
      done;
      formula := And(!formula,Imp(Atom(P(index n i j)), !col_only));

      (* We still need diagonals ! *)

      (* South-eastern direction *)
      let diag_only1 = ref True in
      for k = 1 to (n-1) do
        if (i+k < n && j+k < n) then
          diag_only1 := And(!diag_only1, Not(Atom(P(index n (i+k) (j+k)))))
      done;
      if !diag_only1 <> True then
        formula := And(!formula,Imp(Atom(P(index n i j)), !diag_only1));

      (* North-western direction *)
      let diag_only2 = ref True in
      for k = 1 to (n-1) do
        if (i-k >= 0 && j-k >= 0) then
          diag_only2 := And(!diag_only2, Not(Atom(P(index n (i-k) (j-k)))))
      done;
      if !diag_only2 <> True then
        formula := And(!formula,Imp(Atom(P(index n i j)), !diag_only2));

      (* South-western direction *)
      let diag_only3 = ref True in
      for k = 1 to (n-1) do
        if (i+k < n && j-k >= 0) then
          diag_only3 := And(!diag_only3, Not(Atom(P(index n (i+k) (j-k)))))
      done;
      if !diag_only3 <> True then
        formula := And(!formula,Imp(Atom(P(index n i j)), !diag_only3));

      (* North-eastern direction *)
      let diag_only4 = ref True in
      for k = 1 to (n-1) do
        if (i-k >= 0 && j+k < n) then
          diag_only4 := And(!diag_only4, Not(Atom(P(index n (i-k) (j+k)))))
      done;
      if !diag_only4 <> True then
        formula := And(!formula,Imp(Atom(P(index n i j)), !diag_only4));

    done;
  done;
  !formula;;



(* initialization of tables*)
let t = init_t maxNode in
let ht = init_ht maxNode in
let f = make_queens 5 in
let i = build t ht f in
print_formula f;
debug_print_t t;
print_int i;
print_newline ();
let l = anysat t i in
List.iter (fun (x, b) -> print_int x; print_string ": "; print_bool b; print_newline ()) l;; 
